TRITON_CONTAINER_NAME=registry.gitlab.com/curt-park/tritonserver-ft
FT_BACKEND_VERSION=release/v1.4_tag
TRITON_VERSION=22.12

build:
	git clone https://github.com/triton-inference-server/fastertransformer_backend.git
	cd fastertransformer_backend && \
		git fetch origin $(FT_BACKEND_VERSION)  && \
		git checkout $(FT_BACKEND_VERSION) && \
		cp docker/Dockerfile . && \
		docker build -t $(TRITON_CONTAINER_NAME):$(TRITON_VERSION) .
	rm -rf fastertransformer_backend

push:
	docker push $(TRITON_CONTAINER_NAME):$(TRITON_VERSION)
